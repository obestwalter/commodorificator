import argparse
import logging
import os
import sys
from pathlib import Path

from commodorificator import lib


def main():
    try:
        args = get_args()
        init_logging(args.verbosity)
        lib.run(Path(args.project_path), args.file_pattern, args.command, args.dry_run)
    except Exception as e:
        if isinstance(e, lib.CANT_CONTINUE):
            sys.exit(f"CAN'T CONTINUE: {e.args[0]}")
        raise


def get_args():
    parser = argparse.ArgumentParser(
        description="commodorify your files like CBM-BASIC line numbers."
    )
    parser.add_argument("command")
    parser.add_argument(
        "--project-path",
        default=os.getcwd(),
        help="default is current working directory",
    )
    parser.add_argument(
        "--file-pattern",
        default="*",
        help="regex for files to use - default is all files (*)",
    )
    parser.add_argument("-v", "--verbosity", action="count", default=3)
    parser.add_argument("--dry-run", action="store_true", default=False)
    args = parser.parse_args()
    if args.command not in lib.Commodorificator.COMMANDS:
        raise lib.CANT_CONTINUE(
            f"unknown command ({args.command}) - "
            f"use one of {lib.Commodorificator.COMMANDS}"
        )
    return args


def init_logging(verbosity):
    level = {0: "ERROR", 1: "WARNING", 2: "INFO"}.get(verbosity)
    fmt = (
        "%(asctime)s %(filename)s.%(funcName)s:%(lineno)d %(levelname)s: " "%(message)s"
    )
    logging.basicConfig(level=level or "DEBUG", format=fmt)
