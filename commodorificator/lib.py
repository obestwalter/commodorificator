import logging
import os
import re
from pathlib import Path
from typing import Union

log = logging.getLogger(__name__)


def run(projectPath: Path, filePattern: str, command: str, dryRun: bool):
    paths = get_file_paths(projectPath, filePattern)
    if len(paths) < 2:
        raise CANT_CONTINUE(
            f"not enough to work with matching '{filePattern}' "
            f"in '{projectPath}': {paths}"
        )
    c64 = Commodorificator(paths)
    try:
        getattr(c64, command)(dryRun=dryRun)
    except AttributeError:
        raise CANT_CONTINUE(f"unknown command: {command} (use work or rest)")


class Commodorificator:
    COMMANDS = ["work", "rest"]

    def __init__(self, paths):
        assert len(set(p.parent for p in paths)) == 1
        self.parent = paths[0].parent
        self.stems = [p.stem for p in paths]
        self.prefix = os.path.commonprefix(self.stems)
        self.pairs = [[p.name, p.name] for p in sorted(paths)]
        log.info(f"initialized with {[p[0] for p in self]}")

    def __iter__(self):
        for pair in self.pairs:
            yield pair

    def __len__(self):
        return len(self.pairs)

    def rest(self, dryRun=False):
        self.re_index(gaps=False)
        self.consolidate(dryRun)

    def work(self, dryRun=False):
        self.re_index(gaps=True)
        self.consolidate(dryRun)

    def re_index(self, gaps: bool):
        factor = 10 if gaps else 1
        padding = len(str(len(self) * factor))
        for idx, pair in enumerate(self, start=1):
            newIdx = idx * 10 if gaps else idx
            pair[1] = make_new_name(pair[0], self.prefix, newIdx, padding)

    def consolidate(self, dryRun):
        srcPad = max([len(p[0]) for p in self])
        dstPad = max([len(p[1]) for p in self])
        for pair in self:
            msg = "[DRY RUN] " if dryRun else ""
            log.info(f"{msg}{pair[0]: <{srcPad}} -> {pair[1]: <{dstPad}}")
            if not dryRun and pair[0] != pair[1]:
                oldPath = self.parent / pair[0]
                newPath = self.parent / pair[1]
                oldPath.rename(newPath)


def make_new_name(name: str, prefix: str, newIdx: int, padding: int):
    oldIdx = grab_idx(name, prefix)
    if oldIdx is None:
        rest = name.split(prefix)[-1] if prefix else name
    else:
        rest = name.split(str(oldIdx))[-1]
    newName = f"{prefix}{newIdx:0{padding}}-{rest}"
    return newName


def get_file_paths(path: Path, pattern: str):
    log.info(f"looking for files fitting {pattern} in {path}")
    return [e for e in Path(path).glob(pattern) if e.is_file()]


def grab_idx(name: str, prefix: str = "") -> Union[None, str]:
    pattern = fr"\A{prefix}([0-9]+-)"
    match = re.search(pattern, name)
    if match:
        return match.group(1)


class CANT_CONTINUE(Exception):
    """Name purely for sentimental reasons."""
