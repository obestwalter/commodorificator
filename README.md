**NOTE:** a friend just stumbled over this and gave me some very good feedback (Thank you Stefan :)). I hadn't looked at this from the perspective of people potentially trying to use this in "the real world", as this is marked as a teaching project. Looking at it through these glasses, his remarks made a lot of sense and I also see a lot of things that are objectively horrible from that perspective. So here's a friendly warning: 

**DON'T USE THIS IN PRODUCTION!**

# TODOs

If I ever get around to update/improve this - here is a note to myself about what to do:

* safety first: the default are horrible - this should be `--dry-run` first, so basically having to pass a flag ``--wet-run`` to actually make any changes to the file system
* there is a good possibility here to add in config in the vein of combining a simple config with ideas from [automagic configuration](https://gitlab.com/obestwalter/automagic-configuration/-/blob/master/README.md)
* also very good opportunity to talk about refactoring and API-design with this as basis.

# Meet the commodorificator (TM)

![commodorificator logo](logo.png)

**(This is a learning/teaching project. It is part of the materials in my courses.)**

## Why?


Sometimes I want files to be ordered on the file system level in the order they should be opened/explored. One way of doing that is having them contain a number as index by which the file system orders them. For example in order to sort

```text
my-project-folder
   - a-file.txt
   - other-file.txt
   - some-file.txt
```

differently, I add indexes at the beginning to force the wanted order:

```text
my-project-folder
   - 1-some-file.txt
   - 2-other-file.txt
   - 3-a-file.txt
```

When working on such a project I sometimes want to create a new file that is ordered somewhere in between (or re-order the existing files). With an order like above and no tool to help me, I have to re-index all the files after the file I inserted:

```text
my-project-folder
   - 1-some-file.txt
   - 2-new-file.txt
   - 3-other-file.txt
   - 4-a-file.txt
```

Problem is: I am too lazy to do the re-indexing by hand. I also want to be able to only change the position of the one file I want to be somewhere else in the already existing order.

### How having grown up with a Commodore 64 can help

I learned programming on the Commodore 64. This has not just crippled my typing skills for life but I also learned that the Commodore dialect of BASIC you have to prepend the lines of the program manually with a line number of your choice - a simple BASIC program would look like this and the line numbers are part of what the programmer typed:

    1 print "Hello"
    2 goto 1

If you wanted to extend the program and insert a line between 1 and 2 you had a problem. You had to change all numbers yourself to get the new order:

    1 print "Hello."
    2 print "How are you?"
    3 goto 1

To make that less painful you added gaps to be able to insert lines without having to renumber everything like so:

    10 print "Hello"
    20 goto 10

That way you could extend the program like this:

    10 print "Hello"
    20 goto 10
    15 print "How are you?"

... which was then reordered internally by the BASIC interpreter. The next time you type `list` you will get:

    10 print "Hello"
    15 print "How are you?"
    20 goto 10

I am not even joking. Those were different times kids. It was already much better than [having to punch holes into cards, though ...](https://www.youtube.com/watch?v=KG2M4ttzBnY)

## What the commodorificator (TM) does

The commodorificator (TM) automates the BASIC line number philosophy by providing the user with a command line based, configurable, highly streamlined, professional workflow (TM?).

### Remark on lexicographic orderings

One thing we need to take into account for this approach to work on the file system level is that the ordering of file names is lexicographical, which means that a filename `100-something.txt` would be sorted before `2-something.txt` because the file system sort method looks at each individual character and is unaware that we want it to compare `100` with `2` to sort the file indexed with `2` to come first. To make this work anyway we pad our indexes with zeros like this: `100-something.txt`, `002-something.txt`. This way sorting will work correctly although it is unaware of our indexing approach.

### The two modes: work and rest

#### work mode

```text
$ commodorify work [--project-path </path/to/project>] [--file-pattern 'foo-*.py'] --dry-run
```

* if no `--project-path` is given the current working directory is used.
* if no `--file-pattern` is given all files in the project path are modified.

This does the following:

* All \[specified\] files in the project path are sorted lexicographically according to their current names (just like most file systems do)
* The files are indexed according to their current sorting, like described above
* The files are renamed on the file system if, except `--dry-run` was passed - then it will output what would be done.
* in old C64 fashion the indexes will be multiples of 10 to have room to insert new files or reorder files by changing their number
* if `--file-pattern` contains a common prefix (e.g. `foo-`), the indexes will be inserted after that common prefix instead of at the beginning

To commodorify a new project, it doesn't matter if the files are already using numbered indexes or not. When **work** mode was activated, they will have indexes. It also doesn't matter what the indexes looked like before, when they existed, they will all be evened out to multiples of ten.

#### rest mode

```text
$ commodorify rest [--project-path </path/to/project>] [--file-pattern 'foo-*.py'] --dry-run
```

This does the following:

* all files in the project path that contain any kind of numbers will be assumed to be part of the indexed work files. If this is not the case, `--file-pattern` needs to be specified
* the system is at rest, so no gaps are necessary. The files will be re-indexed keeping the order, but using sequential numbers with just the necessary amount of zero padding.
