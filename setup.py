from setuptools import setup, find_packages

setup(
    name="commodorificator",
    extras_require={"tests": ["pytest", "pytest-cov", "hypothesis"]},
    packages=find_packages(),
    entry_points={"console_scripts": ["commodorify = commodorificator.cli:main"]},
)
