"""Tests that use a lot of the internal machinery.

They can't be seen as unit tests anymore, but aren't quite functional
tests yet either.
"""
import re
from operator import itemgetter
from pathlib import Path

import hypothesis.strategies as st
import pytest
from hypothesis import given, settings, Verbosity

from commodorificator.lib import Commodorificator


PREFIX = "pre-"
REGEX_STR = rf"\A{PREFIX}[a-z\-]*\.[a-z][a-z][a-z]\Z"


@settings(max_examples=64, verbosity=Verbosity.quiet, use_coverage=True)
@given(names=st.sets(st.from_regex(REGEX_STR), min_size=2))
@pytest.mark.parametrize("gaps, factor", ((False, 1), (True, 10)))
def test_re_index(names, gaps, factor):
    def assert_invariants():
        assert all(p[0] == op[0] for p, op in zip(c64, oldPairs))
        assert all(p[0].startswith(PREFIX) for p in c64)
        assert count == len(c64)

    c64 = Commodorificator([Path(pn) for pn in names])
    count = len(names)
    oldPairs = c64.pairs[:]
    assert all(p[0] == p[1] for p in c64)
    assert_invariants()

    c64.re_index(gaps=gaps)

    assert_invariants()
    assert all(p[0] != p[1] for p in c64)
    pat = rf"\A{c64.prefix}([0-9]+)*[a-z\-]*\.[a-z][a-z][a-z]\Z"
    indexes = [re.search(pat, pair[1]).group(1) for pair in c64]
    assert int(indexes[-1]) == len(c64) * factor
    sortedByOldName = sorted(c64, key=itemgetter(0))
    sortedByNewName = sorted(c64, key=itemgetter(1))
    assert sortedByOldName == sortedByNewName


@pytest.mark.parametrize("dryRun", (False, True))
@pytest.mark.parametrize(
    "method, names, results",
    (
        ("rest", ["1-a", "b", "c"], ["1-a", "2-b", "3-c"]),
        ("rest", ["3-a", "b", "c"], ["1-a", "2-b", "3-c"]),
        ("rest", ["a", "b", "c"], ["1-a", "2-b", "3-c"]),
        ("rest", ["a", "b", "c"], ["1-a", "2-b", "3-c"]),
        ("work", ["a", "b", "c"], ["10-a", "20-b", "30-c"]),
        ("work", ["3-a", "5-b", "9-c"], ["10-a", "20-b", "30-c"]),
        ("work", ["1-a", "2-b", "3-c"], ["10-a", "20-b", "30-c"]),
    ),
)
def test_states(caplog, tmpPath, method, names, results, dryRun):
    caplog.set_level("INFO")
    paths = [tmpPath / n for n in names]
    for p in paths:
        p.write_text("")
    c64 = Commodorificator([p / p for p in paths])
    getattr(c64, method)(dryRun)
    for n, r in zip(names, results):
        assert n in caplog.text and r in caplog.text
    if dryRun:
        assert "[DRY RUN]" in caplog.text
    else:
        assert "[DRY RUN]" not in caplog.text
        assert sorted(list(tmpPath.iterdir())) == [
            tmpPath / result for result in results
        ]
