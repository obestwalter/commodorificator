"""Tests that can be seen as testing isolated units.

(by whatever fitting definition of "unit" in the given context).
"""
from pathlib import Path

import pytest

from commodorificator import lib


def test_get_file_paths():
    """Why is this a unit test although it is touching the file system?

    Because this is the unit I am interested in atm and I can test it in isolation.

    I don't care if it is touching the file system or whatever, I only care if I
    can rely on what I am using and trust its functionality.
    e.g.: I trust that the + operator works and I also trust that the stdlib code
    I am using works (in this case pathlib grabbing some paths). I can rely on it
    and my own project structure to provide me with some interesting data to have
    a simple check.

    I find this Kent Beck quote helpful for this:

    > There are tests where a failure points directly to the problem.
    > These tests are tests either of well-understood abstractions or using
    > only well-understood abstractions.
    > Then there are tests where a failure indicates a genuine problem,
    > but where oh where?

    First are unit tests, second are integration or functional tests.
    """
    here = Path(__file__).parent
    paths = lib.get_file_paths(here, "test_*.py")
    assert len(paths) > 1
    assert all(p.is_file() for p in paths)
    assert all(p.name.startswith("test_") for p in paths)
    assert all(p.name.endswith(".py") for p in paths)


@pytest.mark.parametrize(
    "name, prefix, expectation",
    (
        ("", "", None),
        ("23", "", None),
        ("00023", "", None),
        ("23-foo.txt", "", "23-"),
        ("23-24", "", "23-"),
        ("023-24", "", "023-"),
        ("bla12", "", None),
        ("bla-23-foo100.txt", "bla-", "23-"),
        ("bl100-23-bl100-24.txt", "bl100-", "23-"),
        ("bl100-20989-baz100.txt", "bl100-", "20989-"),
        ("bl100-bar", "bl100-", None),
    ),
)
def test_grab_idx(name, prefix, expectation):
    """Typical example based test. Nice and boring.

    The interesting stuff is in the examples.

    TODO to enhance this with a property based test.
    """
    assert expectation == lib.grab_idx(name, prefix)
