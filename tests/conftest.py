from pathlib import Path

import pytest


@pytest.fixture
def tmpPath(tmpdir) -> Path:
    with tmpdir.as_cwd():
        return Path(tmpdir)
