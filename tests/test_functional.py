"""Tests that run the tool like the user would.

To test some functionality we use these very test modules.

... because why not?
"""
import subprocess
from pathlib import Path

import pytest

HERE = Path(__file__).parent
INTERESTING_STUFF = [
    "[DRY RUN]",
    "conftest.py",
    "10-conftest.py",
    "test_unit.py",
    "40-test_unit.py",
]


class Runner:
    COMMAND_NAME = "commodorify"

    def __init__(self, cwd):
        self.cwd = cwd

    def __call__(self, args=None, otherCwd=None):
        cmd = [self.COMMAND_NAME]
        if args:
            cmd += args
        cp = subprocess.run(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            cwd=otherCwd or self.cwd,
        )
        return cp.returncode, cp.stdout.decode(), cp.stderr.decode()


@pytest.fixture(name="runner")
def load_commodorify_8_1(tmpPath) -> Runner:
    return Runner(tmpPath)


def test_wrong_usage(runner):
    ret, out, err = runner()
    assert ret == 2
    assert not out
    assert "usage:" in err


def test_dry_run(runner):
    ret, out, err = runner(["work", "--dry-run"], otherCwd=HERE)
    assert ret == 0
    assert not out
    assert all(txt in err for txt in INTERESTING_STUFF)


SAME = object()


@pytest.mark.parametrize(
    "command, inFiles, outFiles",
    (
        ("work", ["a.txt", "b.txt", "c.txt"], ["10-a.txt", "20-b.txt", "30-c.txt"]),
        ("work", ["10-a.txt", "20-b.txt", "30-c.txt"], SAME),
        ("rest", ["a.txt", "b.txt", "c.txt"], ["1-a.txt", "2-b.txt", "3-c.txt"]),
        ("rest", ["1-a.txt", "2-b.txt", "3-c.txt"], SAME),
        (
            "rest",
            ["13-a.txt", "23-b.txt", "42-c.txt"],
            ["1-a.txt", "2-b.txt", "3-c.txt"],
        ),
    ),
)
def test_work_with_some_files(runner, command, inFiles, outFiles):
    outFiles = inFiles if outFiles is SAME else outFiles
    for idx, name in enumerate(inFiles):
        (runner.cwd / name).write_text(str(idx))
    ret, *_ = runner([command])
    assert ret == 0
    if inFiles != outFiles:
        for name in inFiles:
            assert not (runner.cwd / name).exists()
    for idx, name in enumerate(outFiles):
        path = runner.cwd / name
        assert path.is_file()
        assert int(path.read_text().strip()) == idx
